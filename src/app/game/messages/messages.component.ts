import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
@Input() pWeapon: any;
@Input() pSelected: boolean;
@Input() cWeapon: any;
@Input() msg: string;
@Input() pPoints: number;
@Input() cPoints: number;
@Input() res: string;

  constructor() { }

  ngOnInit() {
  }

}
