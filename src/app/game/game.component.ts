import { Component, OnInit } from '@angular/core';

enum weapons {
  rock = 'rock',
  paper = 'paper',
  scissor = 'scissor'
}
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  playerWeapon: any;
  playerSelected = false;
  computerWeapon: any;
  count = 0;
  weapon = weapons;
  playerPoints = 0;
  computerPoints = 0;
  result: string;
  message: string;
  randomNum: number;
  constructor() { }

  ngOnInit() {
  }

  clear() {
    this.playerSelected = false;
    this.computerPoints = 0;
    this.playerPoints = 0;
    this.count = 0;
    this.message = '';
    this.result = '';
  }

  winnerLoser() {
    if (this.computerPoints === this.playerPoints) {
      this.result = 'You are both winners of the game!';
    } else if (this.computerPoints < this.playerPoints) {
      this.result = 'You WON the game!';
    } else {
      this.result = 'You LOST the game!';
    }
  }

  selectWeapon(selectedWeapon) {
    this.playerSelected = true;
    this.playerWeapon = selectedWeapon;
    setTimeout(() => {
      if (Math.floor(Math.random() * 3) === 0) {
        this.computerWeapon = weapons.rock;
      } else if (Math.floor(Math.random() * 3) === 1) {
        this.computerWeapon = weapons.paper;
      } else {
        this.computerWeapon = weapons.scissor;
      }
      this.play();
    }, 200);
  }

  checkResult() {
    if (this.computerWeapon === 'rock') {
      if (this.playerWeapon === 'scissor') {
        this.computerPoints++;
        this.message = 'Computer Won!';
      } else if (this.playerWeapon === 'rock') {
        this.message = 'Tie';
      } else {
        this.playerPoints++;
        this.message = 'You Won!';
      }
    }
    if (this.computerWeapon === 'paper') {
      if (this.playerWeapon === 'rock') {
        this.computerPoints++;
        this.message = 'Computer Won!';
      } else if (this.playerWeapon === 'paper') {
        this.message = 'Tie';
      } else {
        this.playerPoints++;
        this.message = 'You Won!';
      }
    }
    if (this.computerWeapon === 'scissor') {
      if (this.playerWeapon === 'paper') {
        this.computerPoints++;
        this.message = 'Computer Won!';
      } else if (this.playerWeapon === 'scissor') {
        this.message = 'Tie';
      } else {
        this.playerPoints++;
        this.message = 'You Won!';
      }
    }
  }

  play() {
    this.count++;
    if (this.count < 5) {
      this.checkResult();
    } else {
      this.checkResult();
      this.winnerLoser();
      setTimeout(() => {
        this.clear();
      }, 3000);
    }
  }
}
